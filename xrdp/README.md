# frescobaldi-docker

## Notes on new XRDP setup

Dockerfile:
```
FROM danchitnis/xrdp:fedora-xfce
RUN dnf -y install frescobaldi
```

Run `docker build .` and get the resulting image hash.

Create working dir:

`mkdir -p /tmp/xrdp`

Then, to execute:

```
docker run -d -p 33890:3389 --name xrdp -v /app:/tmp/xrdp <image-hash> username password no
```

Replace username/password with whatever username/password you like.  The "no"
stands for "user gets no sudo rights."

Next, log into localhost:33890 using RDP; this should show an XFCE login
screen.  There, log in with the username/password you have selected above.
That gets you to an XFCE desktop, where you can run frescobaldi.
